import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {doLogin, doRegister} from '../apiActions/auth';
import {withRouter} from 'react-router';

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      firstName: '',
      lastName: '',
      password: '',
      confirmPassword: '',
    };
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleConfirmPasswordChange = this.handleConfirmPasswordChange.bind(this);
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handleConfirmPasswordChange(event) {
    this.setState({ confirmPassword: event.target.value });
  }

  handleFirstNameChange(event) {
    this.setState({ firstName: event.target.value });
  }

  handleLastNameChange(event) {
    this.setState({ lastName: event.target.value });
  }

  login() {
    const { email, firstName, lastName, password, confirmPassword } = this.state;
    if (!email || !password || !confirmPassword || !firstName || !lastName) {
      alert('Asegúrese de diligenciar los campos.');
    } else if (password !== confirmPassword) {
      alert('Asegúrese de que las contraseñas coincidan.');
    } else if (!emailRegex.test(email)) {
      alert('Asegúrese de ingresar un correo válido.');
    } else {
      this.props.doRegister(email, firstName, lastName, password)
          .then(() => {
            console.log('register works!');
            this.props.history.push('/login')
          })
          .catch((err) => {
            alert("Un error ha ocurrido, vuelve a intentar.");
          })
    }
  }

  render() {
    const {
      email,
      password,
      confirmPassword,
      firstName,
      lastName
    } = this.state;
    return (
        <div className="h-full flex-col justify-center items-center px-64 pt-32">
          <p className="text-2xl text-center">Registro</p>
          <form className="bg-white shadow-md rounded px-8 pt-6 pb-8">
            <div className="mb-4">
              <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="username"
              >
                Correo electrónico
              </label>
              <input
                  value={email}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="username"
                  type="email"
                  placeholder="Usuario"
                  onChange={this.handleEmailChange}
              />
            </div>
            <div className="mb-4">
              <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="firstName"
              >
                Nombres
              </label>
              <input
                  value={firstName}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="firstName"
                  type="text"
                  placeholder="Nombres"
                  onChange={this.handleFirstNameChange}
              />
            </div>
            <div className="mb-4">
              <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="lastName"
              >
                Apellidos
              </label>
              <input
                  value={lastName}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  id="lastName"
                  type="text"
                  placeholder="Apellidos"
                  onChange={this.handleLastNameChange}
              />
            </div>
            <div className="mb-4">
              <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="password"
              >
                Contraseña
              </label>
              <input
                  value={password}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                  id="password"
                  type="password"
                  placeholder="******************"
                  onChange={this.handlePasswordChange}
              />
            </div>
            <div className="mb-6">
              <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="confirmPassword"
              >
                Confirmar contraseña
              </label>
              <input
                  value={confirmPassword}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                  id="confirmPassword"
                  type="password"
                  placeholder="******************"
                  onChange={this.handleConfirmPasswordChange}
              />
            </div>
            <div className="flex items-center justify-center">
              <button
                  className="mr-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                  type="button"
                  onClick={() => this.login()}
              >
                Registrarme
              </button>
              <Link
                  to="login"
                  className="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800"
              >
                Entrar
              </Link>
            </div>
          </form>
        </div>
    );
  }
}

export default connect(
    null,
    {
      doRegister
    }
)(withRouter(Register));
