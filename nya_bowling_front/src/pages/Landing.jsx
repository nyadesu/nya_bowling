import React, {Component} from 'react';
import Leaderboard from '../components/Leaderboard';
import {get} from '../utils/network';

class Landing extends Component {

  constructor(props) {
    super(props);
    this.state = {
      scoreData: []
    }
  }

  componentDidMount() {
    get('game/')
        .then((res) => {
          this.setState({
            scoreData: Object.values(res.data.gameMap)
          })
        })
        .catch(() => {
          alert("Ha ocurrido un error, intenta de nuevo más tarde.");
        })
  }

  render() {
    const {
      scoreData
    } = this.state;
    return (
        <div>
          <Leaderboard scoreData={scoreData}/>
        </div>
    );
  }
}

export default Landing;
