import React, { Component } from 'react';
import {Link, withRouter} from 'react-router-dom';
import * as _ from 'lodash';
import Scoreboard from '../../components/Scoreboard';
import update from 'immutability-helper';
import {connect} from 'react-redux';
import {doRegister} from '../../apiActions/auth';
import {saveGame} from '../../apiActions/game';

const generateInitialScoreArray = () => {
  return _.range(12).map(() => ({
    firstShot: undefined,
    secondShot: undefined
  }));
};

class RecordGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      turn: 0,
      tryShoot: 0,
      scoreArray: generateInitialScoreArray(),
      pinsDown: 0,
      lastPinsDown: 0,
      strikeFlag: false,
      spareFlag: false,
      finish: false
    };
    this.handlePinsDownChanged = this.handlePinsDownChanged.bind(this);
  }

  handlePinsDownChanged(event) {
    this.setState({ pinsDown: Number(event.target.value) });
  }

  doShoot() {
    const {
      turn,
      tryShoot,
      pinsDown,
      scoreArray,
      lastPinsDown,
      strikeFlag,
      spareFlag
    } = this.state;

    if (turn < 9) {
      // check if it is not last game
      if (tryShoot == 0 && pinsDown == 10) {
        // check if strike
        console.log('strike');
        this.setState({
          scoreArray: update(scoreArray, {
            [turn]: {
              firstShot: { $set: pinsDown },
              secondShot: { $set: 0 }
            }
          }),
          pinsDown: 0,
          lastPinsDown: 0,
          tryShoot: 0,
          turn: turn + 1
        });
      } else if (tryShoot == 1 && pinsDown + lastPinsDown == 10) {
        // check if spare
        console.log('spare');
        this.setState({
          scoreArray: update(scoreArray, {
            [turn]: {
              secondShot: { $set: pinsDown }
            }
          }),
          pinsDown: 0,
          lastPinsDown: 0,
          tryShoot: 0,
          turn: turn + 1
        });
      } else {
        console.log('else');
        this.setState({
          scoreArray: update(scoreArray, {
            [turn]: {
              [tryShoot == 0 ? 'firstShot' : 'secondShot']: { $set: pinsDown }
            }
          }),
          pinsDown: 0,
          lastPinsDown: pinsDown != 10 && tryShoot == 0 ? pinsDown : 0,
          tryShoot: tryShoot == 1 ? 0 : 1,
          turn: tryShoot == 1 ? turn + 1 : turn
        });
      }
    } else {
      // last turn, can repeat!

      if (tryShoot == 0 && pinsDown == 10) {
        // check if strike
        console.log('strike');
        this.setState(prev => {
          return {
            finished:
              (prev.strikeFlag && prev.turn == 11) ||
              (prev.spareFlag && prev.turn == 10)
                ? true
                : false,
            scoreArray: update(prev.scoreArray, {
              [prev.turn]: {
                firstShot: { $set: prev.pinsDown },
                secondShot: { $set: 0 }
              }
            }),
            pinsDown: 0,
            lastPinsDown: 0,
            tryShoot: 0,
            turn: prev.turn + 1,
            strikeFlag: prev.turn == 9 ? true : false
          };
        });
      } else if (tryShoot == 1 && pinsDown + lastPinsDown == 10) {
        // check if spare
        console.log('spare');
        this.setState(prev => {
          return {
            finished:
              (prev.strikeFlag && prev.turn == 11) ||
              (prev.spareFlag && prev.turn == 10)
                ? true
                : false,
            scoreArray: update(prev.scoreArray, {
              [prev.turn]: {
                secondShot: { $set: prev.pinsDown }
              }
            }),
            pinsDown: 0,
            lastPinsDown: 0,
            tryShoot: 0,
            turn: prev.turn + 1,
            spareFlag: prev.turn == 9 ? true : false
          };
        });
      } else {
        console.log('else');
        debugger;
        console.log(strikeFlag);
        console.log(spareFlag);
        console.log(turn);
        console.log(tryShoot);
        this.setState(prev => {
          return {
            finished:
              ((prev.strikeFlag && prev.turn == 11) ||
                (prev.spareFlag && prev.turn == 10) || (prev.turn == 9 && (prev.pinsDown + prev.lastPinsDown) < 10)) &&
              prev.tryShoot == 1
                ? true
                : false,
            scoreArray: update(prev.scoreArray, {
              [turn]: {
                [tryShoot == 0 ? 'firstShot' : 'secondShot']: { $set: pinsDown }
              }
            }),
            pinsDown: 0,
            lastPinsDown:
              prev.pinsDown != 10 && prev.tryShoot == 0 ? prev.pinsDown : 0,
            tryShoot: prev.tryShoot == 1 ? 0 : 1,
            turn: prev.tryShoot == 1 ? prev.turn + 1 : prev.turn
          };
        });
      }
    }
    console.log(strikeFlag);
    console.log(spareFlag);
    console.log(turn);
    console.log(tryShoot);
  }

  saveGame() {
    this.props.saveGame(this.state.scoreArray)
        .then(() => {
          console.log("save game works");
          this.props.history.push('/')
        })
        .catch(() => {
          alert("Un error ha ocurrido, vuelve a intentar.");
        })
  }

  render() {
    const {
      turn,
      tryShoot,
      scoreArray,
      pinsDown,
      lastPinsDown,
      finished
    } = this.state;
    return (
      <div className="h-full flex-col justify-center items-center px-64 pt-32">
        <div className="bg-blue-100 shadow-md rounded px-8 pt-6 pb-8">
          <div className="text-4xl mb-12">Registrar juego</div>
          <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mt-8">
            <div className="flex flex-col w-full justify-center items-center">
              <div className="text-2xl">MARCADOR</div>
              <div className="mt-6">
                <Scoreboard scoreArray={scoreArray} />
              </div>
            </div>
          </div>
          {
            !finished && (
              <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mt-8">
                <div className="flex flex-row w-full justify-center items-center">
                  <div className="text-2xl">
                    Turno: {`${turn + 1}`} - Tiro: {`${tryShoot + 1}`}
                  </div>
                </div>
                <div className="flex flex-row w-full justify-center items-center mt-4">
                  <div className="text-base">Cantidad de pinos derribados:</div>
                  <div className="inline-block relative w-64 ml-4">
                    <select
                      value={pinsDown}
                      onChange={evt => this.handlePinsDownChanged(evt)}
                      className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline"
                    >
                      <option value={0}>Número de pinos</option>
                      {_.range(1, 11 - lastPinsDown).map(value => (
                        <option value={value}>{value}</option>
                      ))}
                    </select>
                    <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                      <svg
                        className="fill-current h-4 w-4"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                      >
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                      </svg>
                    </div>
                  </div>
                </div>
                <div className="flex flex-row w-full justify-center items-center mt-4">
                  {pinsDown != 0 && (
                    <button
                      onClick={() => this.doShoot()}
                      className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-8 rounded-full"
                    >
                      Terminar tiro
                    </button>
                  )}
                </div>
              </div>
            )
          }
          {finished && (
            <div className="mt-6 flex flex-row w-full justify-center items-center">
              <button
                onClick={() => this.saveGame()}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-4 px-8 rounded-full mr-12"
              >
                ¡Guardar juego!
              </button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(
    null,
    {
      saveGame
    }
)(withRouter(RecordGame));
