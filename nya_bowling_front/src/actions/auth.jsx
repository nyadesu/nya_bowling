import {createActions} from 'redux-actions';
import {LOGIN, LOGOUT} from './types';

export const {
  login,
  logout,
} = createActions(
    {},
    LOGIN,
    LOGOUT,
);
