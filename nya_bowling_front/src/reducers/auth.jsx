import { handleActions } from 'redux-actions';
import { LOAD_KEYWORD_DATA } from '../actions/types';
import { produce } from 'immer';
import {login, logout} from '../actions/auth';
import * as jwt_decode from 'jwt-decode';

const defaultState = {
  token: '',
  email: '',
  userData: {}
};

const reducer = handleActions({
  [login]: (state, { payload }) => {
    const {
      user,
      token
    } = payload;
    return produce(state, draft => {
      draft.email = user;
      draft.token = token;
      draft.userData = jwt_decode(token);
    });
  },
  [logout]: (state, { payload }) => {
    return defaultState;
  }
}, defaultState);

export default reducer;
