import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link, withRouter} from 'react-router-dom';
import { logout } from '../actions/auth';

class Navbar extends Component {

  logout() {
    this.props.logout();
    this.props.history.push('/');
  }

  render() {
    const { token } = this.props;
    return (
      <nav className="flex items-center justify-between flex-wrap bg-purple-600 p-6">
        <div className="flex items-center flex-shrink-0 text-white mr-6">
          <img className="w-12 mr-4" src="/logo.png" alt="" />
          <Link
              to="/"
              className="font-semibold text-xl tracking-tight"
          >
            Nyaa Bowling
          </Link>
        </div>
        {token ? (
          <div>
            <div>
              <Link
                  to="/record-game"
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-8 rounded-full mr-4">
                Registrar juego
              </Link>
              <Link
                  to="/profile"
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-8 rounded-full mr-12">
                Mi Perfil
              </Link>
              <a
                onClick={() => this.logout()}
                className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 lg:mt-0"
              >
                Cerrar sesión
              </a>
            </div>
          </div>
        ) : (
          <div>
            <div>
              <Link
                className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 lg:mt-0"
                to="register"
              >
                Registrarme
              </Link>
              <Link
                className="ml-4 inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 lg:mt-0"
                to="login"
              >
                Iniciar sesión
              </Link>
            </div>
          </div>
        )}
      </nav>
    );
  }
}

export default connect(
  ({ auth }) => {
    return {
      token: auth.token
    };
  },
  {
    logout
  }
)(withRouter(Navbar));
