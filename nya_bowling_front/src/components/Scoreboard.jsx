import React, { Component } from 'react';
import * as _ from 'lodash';

class Scoreboard extends Component {

  constructor(props) {
    super(props);
  }

  getTurnScore(turnIndex, shotIndex) {
    const {
      scoreArray
    } = this.props;
    let score = scoreArray[turnIndex][shotIndex == 0 ? 'firstShot' : 'secondShot'];
    return score;
  }

  getScoreSum(index) {
    const {
      scoreArray
    } = this.props;
    const {
      firstShot,
      secondShot
    } = scoreArray[index];
    return (firstShot || 0) + (secondShot || 0)
  }

  getTotalTurnScore(index) {
    const {
      scoreArray
    } = this.props;
    const {
      firstShot,
      secondShot
    } = scoreArray[index];
    var accum = (firstShot || 0) + (secondShot || 0);
    if (this.isStrike(index)) {
      accum = accum + this.getScoreSum(index+1);
      accum = accum + this.getScoreSum(index+2);
    }
    if (this.isSpare(index)){
      accum = accum + this.getScoreSum(index+1);
    }
    return accum;
  }

  isStrike(index) {
    const {
      scoreArray
    } = this.props;
    const {
      firstShot,
      secondShot
    } = scoreArray[index];
    return firstShot == 10 && secondShot == 0;
  }

  isSpare(index) {
    const {
      scoreArray
    } = this.props;
    const {
      firstShot,
      secondShot
    } = scoreArray[index];
    return secondShot != 0 && (firstShot + secondShot) == 10;
  }

  getTotalTotalScore() {
    return _.range(10).reduce((acc, val, idx) => acc + this.getTotalTurnScore(idx), 0);
  }

  drawTurnScore(index) {
    return (
        <div className="flex flex-col h-full text-center">
          <div className="flex-row" style={{
            flex: 1,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'stretch'
          }}>
            <div
                className="flex-1 rounded-full bg-blue-200 py-2 mx-1 my-1 px-2"
              style={{
                minHeight: '10px'
              }}
            >
              {this.getTurnScore(index, 0)}
            </div>
            <div
                className="flex-1 rounded-full bg-blue-200 py-2 mx-1 my-1 px-2"
              style={{
                minHeight: '10px'
              }}
            >
              {this.getTurnScore(index, 1)}
            </div>
          </div>
          {
            (index < 10) && (
              <div style={{
                flex: 1,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}>
                {this.getTotalTurnScore(index)}
              </div>
            )
          }
        </div>
    );
  }

  render() {
    return (
      <div className="table w-full">
        <div className="table-row">
          <div className="table-cell bg-gray-400 text-gray-700 px-4 py-2 text-sm">
            Turno
          </div>
          {
            _.range(1,11 + (this.isStrike(9) ? 2 : 0) + (this.isSpare(9) ? 1 : 0) ).map((value) => (
                <div className="table-cell bg-gray-400 text-gray-700 px-10 py-2 text-sm">
                  {value}
                </div>
            ))
          }
          <div className="table-cell bg-gray-400 text-gray-700 px-4 py-2 text-sm">
            Puntaje Total
          </div>
        </div>
        <div className="table-row h-16">
          <div className="table-cell bg-gray-200 text-gray-700 text-sm"></div>
          {
            _.range(0,10 + (this.isStrike(9) ? 2 : 0) + (this.isSpare(9) ? 1 : 0) ).map((value, i) => (
              <div className={`table-cell ${i} ${(i % 2 == 0) ? 'bg-gray-300' : 'bg-gray-200'} text-gray-700 text-sm h-24 flex`}>
                {this.drawTurnScore(value)}
              </div>
            ))
          }
          <div className="table-cell bg-gray-800 text-white text-3xl text-center">
            {this.getTotalTotalScore()}
          </div>
        </div>
      </div>
    );
  }
}

export default Scoreboard;
