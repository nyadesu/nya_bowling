import React, {Component} from 'react';
import Scoreboard from './Scoreboard';

class Leaderboard extends Component {



  render() {
    const {
      scoreData
    } = this.props;
    return (
        <div className="p-8 shadow bg-blue-300">
          {
            scoreData.map((data) => (
                <div className="mt-8 m-8 bg-blue-100 px-8 pl-16 py-8 rounded-l-full">
                  <div>
                    Autor: {data.email}
                  </div>
                  <div>
                    <Scoreboard scoreArray={JSON.parse(data.scoreArray)} />
                  </div>
                </div>
            ))
          }
        </div>
    );
  }
}

export default Leaderboard;
