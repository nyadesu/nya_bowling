import React, {Component} from 'react';
import {Redirect, Route} from 'react-router-dom';
import {connect} from 'react-redux';

const PrivateRoute = ({component: Component, auth, ...rest}) => {
  return (
      <Route
          {...rest}
          render={props => auth.token ?
              (<Component {...props} />) : (<Redirect to="/login"/>)
          }
      />
  );
};

const mapStateToProps = ({auth}) => {
  return {
    auth: auth,
  };
};

export default connect(mapStateToProps)(PrivateRoute);
