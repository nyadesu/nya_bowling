import React from 'react';
import Navbar from './components/Navbar';
import Routes from './routes';
import {BrowserRouter as Router} from 'react-router-dom';

function App() {
  return (
      <Router>
        <div className="h-screen px-0">
          <Navbar/>
          {Routes}
        </div>
      </Router>
  );
}

export default App;
