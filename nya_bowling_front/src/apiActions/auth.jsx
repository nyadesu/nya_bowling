import {login} from '../actions/auth';
import {post} from '../utils/network';

export const doLogin = (email, password) => {
  return (dispatch, store) => {
    return post('auth/login', {
      email,
      password
    })
        .then(res => {
          console.log(res);
          alert('¡Bienvenido!');
          dispatch(login(res.data));
        })
        .catch(err => console.error(err));
  };
};

export const doRegister = (email, firstName, lastName, password) => {
  return (dispatch, store) => {
    return post('auth/register', {
      email,
      firstName,
      lastName,
      password
    })
        .then(res => {
          console.log(res);
          alert('¡Cuenta creada! Ahora puedes iniciar sesión.');
          return Promise.resolve(res.data);
        })
        .catch(err => console.error(err));
  };
};
