import {login} from '../actions/auth';
import {post} from '../utils/network';

export const saveGame = (scoreArray) => {
  return (dispatch, store) => {
    const {
      email
    } = store().auth;
    return post('game/save', {
      scoreArray: JSON.stringify(scoreArray),
      email
    })
        .then(res => {
          console.log(res);
          alert('¡Juego guardado!');
          return Promise.resolve(res.data);
        })
        .catch(err => console.error(err));
  };
};
