import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Landing from './pages/Landing';
import Login from './pages/Login';
import Register from './pages/Register';
import Profile from './pages/User/Profile';
import PrivateRoute from './components/PrivateRoute';
import GameList from './pages/User/GameList';
import RecordGame from './pages/User/RecordGame';

const Routes = (
    <div class="h-full">
      <Route path="/" exact strict component={Landing}/>
      <Route path="/login" exact strict component={Login}/>
      <Route path="/register" exact strict component={Register}/>
      <Switch>
        <PrivateRoute path="/profile" exact strict component={Profile}/>
        <PrivateRoute path="/game-list" exact strict component={GameList}/>
        <PrivateRoute path="/record-game" exact strict component={RecordGame}/>
      </Switch>
    </div>
);

export default Routes;
