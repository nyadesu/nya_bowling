import { Router } from 'express';
import {mongoUrl} from '../config';
import Game from '../models/game';
const router = new Router();

router.get('/', function(req, res, next) {
  try {
    Game.find({}, function(err, games) {
      var gameMap = {};
      games.forEach(function(game) {
        gameMap[game._id] = game;
      });

      res.status(200).send({gameMap});
    });
  } catch (e) {
    console.log(e);
    res.status(500).send({ error: e.message });
  }
});

router.post('/save', async (req, res) => {
  try {
    const { scoreArray, email } = req.body;

    let newGame = new Game({
      email,
      scoreArray
    });
    newGame.save((err, result) => {
      if(err) throw err;
      res.status(200).send(result)
    });
  } catch (e) {
    console.log(e);
    res.status(500).send({ error: e.message });
  }
});

export default router;
