import { Router } from 'express';
import AuthController from '../controllers/authentication.controller';
import UserController from '../controllers/user.controller';
const router = new Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/profile', (req, res, next) => {
  UserController.profile(req, res, next);
});

export default router;
