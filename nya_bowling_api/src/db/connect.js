import mongoose from 'mongoose';
import {mongoUrl} from '../config';

mongoose.Promise = global.Promise;

const connectToDb = async () => {
  try {
    await mongoose.connect(mongoUrl);
    console.info('Connected to mongo!!!');
  } catch (err) {
    console.error('Error reaching mongo instance:', err);
  }
};

export default connectToDb;
