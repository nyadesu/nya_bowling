import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import passportLocalMongoose from  'passport-local-mongoose';

let gameSchema = new Schema({
  scoreArray: String,
  email: String,
});

let Game = mongoose.model('Games', gameSchema);

export default Game;
