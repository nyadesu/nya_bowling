import express from 'express';
import bodyParser from 'body-parser';
import connectToDb from './db/connect';
import passport from 'passport';
import auth from './routes/auth.routes';
import user from './routes/user.routes';
import games from './routes/games.routes';
import User from './models/user';

import {port, secret} from './config';

const passportJWT = require("passport-jwt");
const JWTStrategy   = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

const LocalStrategy = require('passport-local').Strategy;

const server = express();

// Establish connection to mongo instance
connectToDb()
  .then(() => {
    // Setting up middlewares
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({
      extended: false
    }));
    server.use(passport.initialize());

    // Setting up login policy, extracting credentials from payload and
    // authenticating using passport-local-mongoose
    passport.use(new LocalStrategy({
          usernameField: 'email',
          passwordField: 'password'
        },
        User.authenticate()
    ));
    passport.serializeUser(User.serializeUser());
    passport.deserializeUser(User.deserializeUser());

    // Setting up JWT bearer token verification
    passport.use(new JWTStrategy({
          jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
          secretOrKey   : secret
        },
        (jwtPayload, cb) => {
          // Looking up user model by id extracted from JWT payload
          return User.findById(jwtPayload.id)
          .then(user => {
            return cb(null, user);
          })
          .catch(err => {
            return cb(err);
          });
        }
    ));

    server.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });
    server.use('/auth', auth);
    server.use('/game', games);
    server.use('/user', passport.authenticate('jwt', {session: false}), user);
    server.listen(port, () => {
      console.log(`auth service started - ${port}`);
    });
  });

