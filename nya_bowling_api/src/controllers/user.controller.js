const UserController = {};

UserController.profile = async (req, res) => {
  try {
    res.send(req.user);
  } catch (err) {
    res.send('No user Returned');
  }
};

export default UserController;
