import User from '../models/user';
import passport from 'passport';

import jwt from 'jsonwebtoken';
import {secret} from '../config';
// import {generateToken, respond} from '../middleware/authMiddleware';


const AuthenticationController = {};

AuthenticationController.register = async (req, res) => {
  try {
    const {
      email,
      firstName,
      lastName,
      password
    } = req.body;
    User.register(new User({
      username: email,
      firstName: firstName,
      lastName: lastName,
    }), password, function(err, account) {
      if (err) {
        return res.status(500).send('Error trying to create user: ' + err);
      }

      passport.authenticate(
          'local', {
            session: false,
          })(req, res, () => {
        res.status(200).send('User created successfully');
      });
    });
  } catch (err) {
    return res.status(500).send('An error occurred: ' + err);
  }
};

AuthenticationController.login = async (req, res, next) => {
  try {
    const {
      email,
      password
    } = req.body;
    if (!email || !password) {
      return res.status(400).json({
        message: 'Something is not right with your input',
      });
    }
    passport.authenticate('local', {session: false}, (err, user, info) => {
      if (err || !user) {
        return res.status(400).json({
          message: `Something is not right - ${err}`,
          user: user,
        });
      }
      req.login(user, {session: false}, (err) => {
        if (err) {
          res.send(err);
        }
        // generate a signed son web token with the contents of user object and return it in the response
        const token = jwt.sign({
          id: user.id,
          email: user.username,
          firstName: user.firstName,
          lastName: user.lastName
        }, secret);
        return res.json({user: user.username, token});
      });
    })(req, res);
  } catch (err) {
    console.log(err);
  }
};

AuthenticationController.logout = async (req, res) => {
  req.logout();
  res.status(200).send('logged out');
};

export default AuthenticationController;
